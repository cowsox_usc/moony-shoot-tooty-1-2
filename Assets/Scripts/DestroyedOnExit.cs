﻿using UnityEngine;
using System.Collections;
using System;

public class DestroyedOnExit : MonoBehaviour, IUsePool
{
	
    // If we are going to return to a pool, the objectPool adds itself here.
    // This allows us to remove ourselves back to the pool
    ObjectPool objectPool = null;

    public void SetPool(ObjectPool givenObjectPool)
    {
        this.objectPool = givenObjectPool;
    }

    // Called when the object leaves the viewport
    void OnBecameInvisible()
    {
        if (objectPool != null)
        {
			// @TODO: Remove this print once everything works
			print ("returning to pool");
            objectPool.returnObject(gameObject);
        }
        else
        {
			// @TODO: Remove this print once everything works
			print ("destroying: " + gameObject.name);
            Destroy(gameObject);
        }
    }
}
